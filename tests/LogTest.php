<?php
namespace Vespula\Log;
use Vespula\Log\Log;

use PHPUnit\Framework\TestCase;

class LogTest extends TestCase {
    
    protected $logger;
    protected $context_array = [];
    
    public function setUp(): void
    {

        $this->context_array = [
            'name'=>'Foo Bar'
        ];

    }
    
    public function testLog()
    {
        $expected = 'My message with context Foo Bar';
        
        $adapter = $this->getMockBuilder('\Vespula\Log\Adapter\None')->setMethods(['write'])->getMock();
        $adapter->expects($this->once())->method('write')
                ->with('alert', $this->equalTo($expected));
        
        $logger = new Log('none', $adapter);
        
        $level = \Psr\Log\LogLevel::ALERT;
        $message = 'My message with context {name}';
        
        $logger->log($level, $message, $this->context_array);
        

    }
    
    public function testLogMulit()
    {
        $expected = 'My message with context Foo Bar';
        
        $adapter_one = $this->getMockBuilder('\Vespula\Log\Adapter\None')->setMethods(['write'])->getMock();
        $adapter_one->expects($this->once())->method('write')
                ->with('alert', $this->equalTo($expected));
        
        $adapter_two = $this->getMockBuilder('\Vespula\Log\Adapter\None')->setMethods(['write'])->getMock();
        $adapter_two->expects($this->once())->method('write')
                ->with('alert', $this->equalTo($expected));
        
        $logger = new Log('one', $adapter_one);
        $logger->addAdapter('two', $adapter_two);
        
        $level = \Psr\Log\LogLevel::ALERT;
        $message = 'My message with context {name}';
        
        $logger->log($level, $message, $this->context_array);
        

    }
    
    public function testLogMulitSecondTooHigh()
    {
        $expected = 'My message with context Foo Bar';
        
        $adapter_one = $this->getMockBuilder('\Vespula\Log\Adapter\None')->setMethods(['write'])->getMock();
        $adapter_one->expects($this->once())->method('write')
                ->with('alert', $this->equalTo($expected));
        
        $adapter_two = $this->getMockBuilder('\Vespula\Log\Adapter\None')->setMethods(['write'])->getMock();
        $adapter_two->expects($this->never())->method('write')
                ->with('alert', $this->equalTo($expected));
        
        $adapter_two->setMinLevel(\Psr\Log\LogLevel::EMERGENCY);
        
        $logger = new Log('one', $adapter_one);
        $logger->addAdapter('two', $adapter_two);
        
        $level = \Psr\Log\LogLevel::ALERT;
        $message = 'My message with context {name}';
        
        $logger->log($level, $message, $this->context_array);
        

    }
    
    public function testLogArray()
    {
        $context = print_r($this->context_array, true);
        $expected = 'My message with context ' . $context;
        
        $adapter = $this->getMockBuilder('\Vespula\Log\Adapter\None')->setMethods(['write'])->getMock();
        $adapter->expects($this->once())->method('write')
                ->with('info', $this->equalTo($expected));
        
        $logger = new Log('foo', $adapter);
        
        $level = \Psr\Log\LogLevel::INFO;
        $message = 'My message with context {name}';
        
        $logger->log($level, $message, ['name'=>$this->context_array]);
    }
    
    public function testMinLevelMatched()
    {
        $expected = 'My message with context Foo Bar';
        
        $adapter_alert = $this->getMockBuilder('\Vespula\Log\Adapter\None')->setMethods(['write'])->getMock();
        $adapter_alert->expects($this->once())->method('write')
                ->with('alert', $this->equalTo($expected));
        
        $adapter_alert->setMinLevel(\Psr\Log\LogLevel::ALERT);

        $logger = new Log('alert', $adapter_alert);
        
        $level = \Psr\Log\LogLevel::ALERT;
        $message = 'My message with context {name}';
        
        $logger->log($level, $message, $this->context_array);
    }
    
    public function testMinLevelExceded()
    {
        
        $adapter = $this->getMockBuilder('\Vespula\Log\Adapter\None')->setMethods(['write'])->getMock();

        $adapter->setMinLevel(\Psr\Log\LogLevel::NOTICE);
        
        $logger = new Log('none', $adapter);
        
        $level = \Psr\Log\LogLevel::ALERT;
        $message = 'My message with context {name}';
        
        $this->assertNull($logger->log($level, $message, $this->context_array));
    }
    
    public function testGetAdapters()
    {
        $adapter = new \Vespula\Log\Adapter\None();
        $logger = new Log('none', $adapter);
        
        $expected = [
            'none'=>$adapter
        ];
        
        $this->assertEquals($expected, $logger->getAdapters());
    }
    
    public function testGetAdapter()
    {
        $adapter = new \Vespula\Log\Adapter\None();
        $logger = new Log('none', $adapter);
        
        
        $this->assertEquals($adapter, $logger->getAdapter());
    }
    
    public function testGetAdapterMulti()
    {
        $adapter = new \Vespula\Log\Adapter\None();
        $adapter_file = new \Vespula\Log\Adapter\File('/tmp/blah.txt');
        $logger = new Log('none', $adapter);
        
        $logger->addAdapter('file', $adapter_file);
        
        
        $this->assertEquals($adapter_file, $logger->getAdapter('file'));
    }
}