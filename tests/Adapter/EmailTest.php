<?php
namespace Vespula\Log\Adapter;

use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase 
{
    
    
    protected $adapter;
    
    public function setUp(): void
    {
        $this->adapter = new Email('you@example.com', 'me@example.com');
        $this->adapter->setPath(sys_get_temp_dir());
        
    }
    
    public static function tearDownAfterClass(): void
    {
        $tmp_files = glob(sys_get_temp_dir() . '/vespula_log*');
        foreach($tmp_files as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }
    
    public function testEmail()
    {

        $date_c = date('c');
        
        $expected = 'To: you@example.com' . PHP_EOL;
        $expected .= 'Sent: ' . $date_c . PHP_EOL;
        $expected .= 'From: me@example.com' . "\r\n";
        $expected .= 'Subject: Application Log (info)' . PHP_EOL;
        $expected .= str_repeat('-', 80) . PHP_EOL;
        $expected .= '[' . $date_c . ']' . "\t" . '[INFO]' . "\t" . 'FooBar' . PHP_EOL;
        $expected .= str_repeat('=', 80) . PHP_EOL . PHP_EOL;
        
        $filename = date('Ymd\THis') . '.txt';
        $file = sys_get_temp_dir() . '/vespula_log' . $filename;
        
        $this->adapter->write('info', 'FooBar');
        
        $actual = file_get_contents($file);
        
        $this->assertEquals($expected, $actual);
    }
    
    public function testAppendLevelOff()
    {

        $this->adapter->appendLevelOff();
        
        $date_c = date('c');
        
        $expected = 'To: you@example.com' . PHP_EOL;
        $expected .= 'Sent: ' . $date_c . PHP_EOL;
        $expected .= 'From: me@example.com' . "\r\n";
        $expected .= 'Subject: Application Log' . PHP_EOL;
        $expected .= str_repeat('-', 80) . PHP_EOL;
        $expected .= '[' . $date_c . ']' . "\t" . '[INFO]' . "\t" . 'FooBar' . PHP_EOL;
        $expected .= str_repeat('=', 80) . PHP_EOL . PHP_EOL;
        
        $filename = date('Ymd\THis') . '.txt';
        $file = sys_get_temp_dir() . '/vespula_log' . $filename;
        
        $this->adapter->write('info', 'FooBar');
        
        $actual = file_get_contents($file);
        
        $this->assertEquals($expected, $actual);
    }
    
    public function testAppendLevelOn()
    {

        $this->adapter->appendLevelOn();
        
        $date_c = date('c');
        
        $expected = 'To: you@example.com' . PHP_EOL;
        $expected .= 'Sent: ' . $date_c . PHP_EOL;
        $expected .= 'From: me@example.com' . "\r\n";
        $expected .= 'Subject: Application Log (error)' . PHP_EOL;
        $expected .= str_repeat('-', 80) . PHP_EOL;
        $expected .= '[' . $date_c . ']' . "\t" . '[ERROR]' . "\t" . 'FooBar' . PHP_EOL;
        $expected .= str_repeat('=', 80) . PHP_EOL . PHP_EOL;
        
        $filename = date('Ymd\THis') . '.txt';
        $file = sys_get_temp_dir() . '/vespula_log' . $filename;
        
        $this->adapter->write('error', 'FooBar');
        
        $actual = file_get_contents($file);
        
        $this->assertEquals($expected, $actual);
    }
    
    public function testSetHeaderDelimiter()
    {

        $this->adapter->setHeaderDelimiter("\n");
        
        $date_c = date('c');
        
        $expected = 'To: you@example.com' . PHP_EOL;
        $expected .= 'Sent: ' . $date_c . PHP_EOL;
        $expected .= 'From: me@example.com' . "\n";
        $expected .= 'Subject: Application Log (error)' . PHP_EOL;
        $expected .= str_repeat('-', 80) . PHP_EOL;
        $expected .= '[' . $date_c . ']' . "\t" . '[ERROR]' . "\t" . 'FooBar' . PHP_EOL;
        $expected .= str_repeat('=', 80) . PHP_EOL . PHP_EOL;
        
        $filename = date('Ymd\THis') . '.txt';
        $file = sys_get_temp_dir() . '/vespula_log' . $filename;
        
        $this->adapter->write('error', 'FooBar');
        
        $actual = file_get_contents($file);
        
        $this->assertEquals($expected, $actual);
    }
   
}