<?php
namespace Vespula\Log\Adapter;
use PHPUnit\Framework\TestCase;
class ErrorLogTest extends TestCase {
    
    protected $filename = '/tmp/vespula_log.txt';


    public function setUp(): void 
    {
        if (file_exists($this->filename)) {
            unlink($this->filename);
        }
    }
    
    public function testWriteFile()
    {

        $level = 'info';
        $message = 'my message';
        $timestamp = date('c');
        $adapter = new \Vespula\Log\Adapter\ErrorLog(3, $this->filename);
        $adapter->write($level, $message);
        
        $level = 'INFO';
        $expected = "[$timestamp]\t[$level]\t$message" . PHP_EOL;
        $actual = file_get_contents($this->filename);
        $this->assertEquals($expected, $actual);
    }
    
    public function testBadLevel()
    {
        $this->expectException(\Vespula\Log\Exception\InvalidArgumentException::class);
        $adapter = new \Vespula\Log\Adapter\ErrorLog(6, $this->filename);
        // This should not be written. Exception should be thrown first.
        $adapter->write('info', 'some message');
    }

    public function testSetTimestamp()
    {
        $level = 'info';
        $message = 'my message';
        $dt = new \DateTime("now", new \DateTimeZone('UTC'));
        $timestamp = $dt->format('c');
        $adapter = new \Vespula\Log\Adapter\ErrorLog(3, $this->filename);
        $adapter->setTimezone('UTC');
        $adapter->write($level, $message);
        
        $level = 'INFO';
        $expected = "[$timestamp]\t[$level]\t$message" . PHP_EOL;
        $actual = file_get_contents($this->filename);
        $this->assertEquals($expected, $actual);
    }

}