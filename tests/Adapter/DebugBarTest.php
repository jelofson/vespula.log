<?php
namespace Vespula\Log\Adapter;
use PHPUnit\Framework\TestCase;
class DebugBarTest extends TestCase 
{
    
    
    protected $debugbar;
    protected $collector;

    public function setUp(): void
    {
        $this->debugbar = new \DebugBar\StandardDebugBar();
        $this->collector = $this->debugbar['messages'];
    }
    
    public function testWriteFile()
    {
        
        $adapter = new \Vespula\Log\Adapter\DebugBar($this->collector);
        
        $level = 'info';
        $message = 'my message';
        $timestamp = date('c');
        
        $adapter->write($level, $message);
        
        $level = 'INFO';
        $actual = $this->collector->getMessages()[0]['message'];
        $expected = "[$timestamp]\t[$level]\t$message";
       
        $this->assertEquals($expected, $actual);
    }
   
}