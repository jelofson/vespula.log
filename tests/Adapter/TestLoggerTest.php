<?php

declare(strict_types=1);

namespace Vespula\Log\Adapter;

use Generator;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;
use ReflectionClass;
use Vespula\Log\Log;

use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertSame;
use function PHPUnit\Framework\assertTrue;
use function sprintf;
use function str_replace;
use function substr;
use function ucwords;

class TestLoggerTest extends TestCase
{
    /** @var TestLogger */
    protected $adapter;

    public function dataLevels(): Generator
    {
        $levels = $this->getLogLevels();
        foreach ($levels as $testLevel) {
            yield $testLevel => [$testLevel];
        }
    }

    public function dataLevelVsLevel(): Generator
    {
        $levels = $this->getLogLevels();
        foreach ($levels as $testLevel) {
            foreach ($levels as $writeLevel) {
                yield $testLevel . ' vs ' . $writeLevel => [$writeLevel, $testLevel];
            }
        }
    }

    public function dataHasRecordMethods(): Generator
    {
        $methods = [
            'exact'    => ['', static function ($string) { return $string; }],
            'contains' => [
                'ThatContains',
                static function ($string): string { return substr($string, 1, -1); },
            ],
            'matches'  => [
                'ThatMatches',
                static function ($string): string { return sprintf('/^%s$/', $string); },
            ],
            'passes'   => [
                'ThatPasses',
                static function ($string): callable {
                    return static function ($record) use ($string): bool {
                        return $record['message'] === $string;
                    };
                },
            ],
        ];
        foreach ($methods as $name => $baseArgs) {
            foreach ($this->dataLevels() as $levelName => $levelValue) {
                $baseArgs[] = $levelValue[0];
                yield $name . ' via ' . $levelName => $baseArgs;
            }
        }
    }

    /**
     * @param string $writeLevel
     * @param string $testLevel
     *
     * @dataProvider dataLevelVsLevel
     */
    public function testHasRecordsLevelVsLevel(string $writeLevel, string $testLevel): void
    {
        $this->adapter->write($writeLevel, __FUNCTION__);
        $expected = $writeLevel === $testLevel;
        assertSame($expected, $this->adapter->hasRecords($testLevel));
        assertSame($expected, $this->adapter->{'has' . ucwords($testLevel) . 'Records'}());
    }

    /**
     * @param string $testLevel
     *
     * @dataProvider dataLevels
     */
    public function testHasRecordsInverse(string $testLevel): void
    {
        assertFalse($this->adapter->hasRecords($testLevel));
        assertFalse($this->adapter->{'has' . ucwords($testLevel) . 'Records'}());
    }

    /**
     * @param string   $method
     * @param callable $messageFilter
     * @param string   $writeLevel
     *
     * @dataProvider dataHasRecordMethods
     */
    public function testHasRecord(string $method, callable $messageFilter, string $writeLevel): void
    {
        $message = __FUNCTION__;
        $this->adapter->write($writeLevel, $message);
        $testArg = $messageFilter($message);
        assertTrue($this->adapter->{'hasRecord' . $method}($testArg, $writeLevel));
        assertTrue($this->adapter->{'has' . ucwords($writeLevel) . $method}($testArg));
    }

    /**
     * @dataProvider dataLevels
     */
    public function testReset(string $level): void
    {
        $this->adapter->write($level, __FUNCTION__);
        $this->adapter->reset();
        assertFalse($this->adapter->hasRecords($level));
    }

    /**
     * @dataProvider dataLevels
     */
    public function testContextIsExpanded(string $writeLevel): void
    {
        $keyName         = 'funcName';
        $template        = "this is {{$keyName}}";
        $context         = [$keyName => __FUNCTION__];
        $expandedMessage = str_replace("{{$keyName}}", $context[$keyName], $template);

        $logger = new Log('test', $this->adapter);
        $logger->log($writeLevel, $template, $context);

        assertTrue($this->adapter->hasRecords($writeLevel));
        assertTrue($this->adapter->hasRecord($expandedMessage, $writeLevel));
        assertTrue($this->adapter->hasRecordThatContains($expandedMessage, $writeLevel));
        assertTrue($this->adapter->hasRecordThatContains($context[$keyName], $writeLevel));
        assertTrue($this->adapter->hasRecordThatMatches('/^this is .+$/', $writeLevel));
        assertTrue(
            $this->adapter->hasRecordThatPasses(
                static function ($record) use ($expandedMessage): bool {
                    return $record['message'] === $expandedMessage;
                },
                $writeLevel
            )
        );
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->adapter = new TestLogger();
    }

    /**
     * @return array<string, string>
     */
    protected function getLogLevels(): array
    {
        $ref    = new ReflectionClass(LogLevel::class);
        $levels = [];
        foreach ($ref->getConstants() as $contName => $contValue) {
            $levels[$contName] = $contValue;
        }

        return $levels;
    }

}
