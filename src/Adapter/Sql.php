<?php
declare(strict_types=1);

namespace Vespula\Log\Adapter;

use Exception;
use PDO;
use Vespula\Log\Exception\InvalidColumnMapException;
use function array_key_exists;
use function implode;

/**
 * Log adapter for logging to a database
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class Sql extends AbstractAdapter
{
    const DRIVER_SQLITE = 'sqlite';
    /**
     * @var PDO the Pdo object
     */
    protected $pdo;

    /**
     *
     * @var string The log table name
     */
    protected $table;

    /**
     * @var string The default date format used in the php date() function
     */
    protected $date_format = 'Y-m-d H:i:s';

    /**
     * A map of columns in the table. Must have keys level, message,
     * and timestamp
     *
     * @var array<string, string> Column map
     */
    protected $cols = [
        'level'=>'level',
        'message'=>'message',
        'timestamp'=>'timestamp'
    ];

    /**
     * Constructor
     *
     * @param PDO $pdo
     * @param string $table
     */
    public function __construct(PDO $pdo, string $table)
    {
        $this->pdo = $pdo;
        $this->table = $table;
    }

    /**
     * Set the column map array
     *
     * @param array<string, string> $cols
     * @return void
     * @throws InvalidColumnMapException
     */
    public function setCols(array $cols)
    {
        $this->cols = $cols;

        // Make sure the right keys are in the cols array
        if (! array_key_exists('level', $this->cols)) {
            throw new InvalidColumnMapException('Missing `level` column');
        }
        if (! array_key_exists('message', $this->cols)) {
            throw new InvalidColumnMapException('Missing `message` column');
        }
        if (! array_key_exists('timestamp', $this->cols)) {
            throw new InvalidColumnMapException('Missing `timestamp` column');
        }
    }

    /**
     * Write the log file to the db table
     *
     * @param string $level
     * @param string $message
     * @return true
     * @throws Exception
     */
    public function write(string $level, string $message)
    {
        $level_col = $this->cols['level'];
        $message_col = $this->cols['message'];
        $timestamp_col = $this->cols['timestamp'];

        $sql = "INSERT INTO {$this->table} ({$level_col}, {$message_col}, {$timestamp_col}) values (:level, :message, :timestamp)";
        $statement = $this->pdo->prepare($sql);
        $params = [
            ':level'=>$level,
            ':message'=>$message,
            ':timestamp'=>$this->getTimestamp()
        ];
        $success = $statement->execute($params);

        // PDO itself can throw exceptions (PDOException)
        // see https://www.php.net/manual/en/pdo.setattribute.php with PDO::ATTR_ERRMODE attribute
        if ($success === false) {
            $errorInfo = $statement->errorInfo();
            $message = '';
            if (isset($errorInfo[0])) {
                $message .= '(' . $errorInfo[0] . ')';
            }
            if (isset($errorInfo[2])) {
                $message .= ' ' . $errorInfo[2];
            }
            throw new Exception('Error inserting log data. Possible error information: ' . $message);
        }

        return true;
    }

    public function getCount(): int
    {
        $sql = "SELECT count(*) from {$this->table}";
        $stmt = $this->pdo->query($sql);
        // TODO: maybe here should be thrown Exception
        if ($stmt === false) {
            return 0;
        }

        return (int)$stmt->fetchColumn();
    }

    /**
     * @param int $rows Pagination limit, use 0 to get all entries
     * @param int $page
     * @param string[] $order_by Field names to sort by, ex: ["timestamp", "level DESC"]
     * @return array<array<string, mixed>>
     */
    public function getEntries(int $rows = 0, int $page = 0,  array $order_by = []): array
    {

        $sql = "SELECT * FROM {$this->table}";
        $order_by_sql = '';
        if ($order_by) {
            $order_by_sql = ' ORDER BY ' . implode(', ', $order_by);
        }
        $sql .= $order_by_sql;

        $offset = false;
        if ($rows > 0) {
            $sql .= ' limit :limit';
            if ($page > 0) {
                $sql .= ' offset :offset';
                $offset = ($page - 1) * $rows;
            }
        }

        $stmt = $this->pdo->prepare($sql);
        if ($rows) {
            $stmt->bindParam(':limit', $rows, PDO::PARAM_INT);
        }
        if ($offset !== false) {
            $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
        }

        // TODO: maybe here should be thrown Exception
        if ($stmt->execute() === false) {
            return [];
        }

        return $stmt->fetchAll(PDO::FETCH_ASSOC) ?: [];
    }

}
