<?php
declare(strict_types=1);

namespace Vespula\Log\Adapter;

use Vespula\Log\Exception;
use Vespula\Log\Exception\InvalidArgumentException;
use function array_chunk;
use function array_reverse;
use function count;
use function dirname;
use function error_log;
use function explode;
use function file;
use function is_writable;
use function preg_match;
use function str_replace;
use function trim;
use const PHP_EOL;

/**
 * Log adapter for logging to a file
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */

class File extends AbstractAdapter
{
    /**
     * The error_log message type
     */
    const MESSAGE_TYPE = 3;

    /**
     * The absolute path to the log file
     * @var string
     */
    protected $file;

    /**
     * The default regex for finding the start of a log entry line (entries can span multiple lines).
     * Normally, this is an ISO 8601 timestamp (2020-01-02T22:00:00-6:00)
     * @var string
     */
    protected $line_start_pattern = '/^\[[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\+|-)[0-9]{2}:[0-9]{2}\]/';

    /**
     * Constructor
     *
     * @param string $file Full path to log file.
     * @throws InvalidArgumentException
     */
    public function __construct(string $file)
    {
        if (!is_writable(dirname($file))) {
            throw new InvalidArgumentException('Log file directory is not writable: ' . $file);
        }
        $this->file = $file;
    }

    /**
     * Write the log message
     *
     * @param string $level
     * @param string $message
     * @return bool
     */
    public function write(string $level, string $message): bool
    {
        $timestamp = $this->getTimestamp();
        $message = $this->buildMessage($level, $message, $timestamp) . PHP_EOL;

        return error_log($message, self::MESSAGE_TYPE , $this->file);

    }

    /**
     * @return int
     * @throws Exception
     */
    public function getCount()
    {
        $all = $this->getEntries();
        return count($all);
    }


    /**
     * Get log entries from the text file
     *
     * @param int $rows The maximum entries to return. 0 for all. Default 0.
     * @param int $page
     * @param bool $lifo Last In First Out. Show most recent entries first. Default true
     * @return array<array<string, string>> The log entries separated by keys (timestamp, level, message)
     * @throws Exception
     */
    public function getEntries(int $rows = 0, int $page = 0, bool $lifo = true): array
    {
        $lines = file($this->file);
        // TODO: may be there should be more error handling
        if ($lines === false) {
            return [];
        }
        /** @var string[] $lines */
        $entries = $this->fetchEntriesAsArray($lines, $lifo);
        $entries = $this->getEntriesAsColumns($entries);

        if ($rows > 0) {
            $chunks = array_chunk($entries, $rows);

            if ($page > 0) {
                $pages = count($chunks);
                $index = $page - 1;
                if ($page > $pages) {
                    $index = $pages - 1;
                }
                return $chunks[$index];
            }

            return $chunks[0];
        }

        return $entries;
    }

    /**
     * Set the regex pattern used to find the start of a log entry.
     *
     * @param string $pattern Regex for start of a line
     * @return void
     */
    public function setLineStartPattern(string $pattern)
    {
        $this->line_start_pattern = $pattern;
    }

    /**
     * Get the log entries as a simple array
     *
     * @param string[] $lines The lines from a file() call.
     * @param bool $lifo Last in first out (true or false)
     * @return string[] The log entries. Each entry as one element in the array.
     * @throws Exception
     */
    protected function fetchEntriesAsArray(array $lines, bool $lifo): array
    {
        $entries = [];
        $match = null;
        foreach ($lines as $key=>$line) {
            if (preg_match($this->line_start_pattern, $line)) {
                $match = $key;
            } elseif ($match === null) {
                throw new Exception('Could not match a starting log entry. Check your pattern');
            }
            $entries[$match] = ($entries[$match] ?? '') . $line;
        }

        if ($lifo) {
            return array_reverse($entries);
        }

        return $entries;
    }

    /**
     * Returns the log entries organized by columns (timestamp, level, message). This is based on the
     * predefined message format
     *
     * @param string[] $entries
     * @param string $delimiter How each component is delimited. Timestamp \t level \t message. Default is \t
     * @return array<array<string, string>> Log entries indexed by column
     * @phpstan-param non-empty-string $delimiter
     */
    protected function getEntriesAsColumns(array $entries, string $delimiter = "\t"): array
    {
        $data = [];
        /** @var array<int, string> $entry_cols */
        $entry_cols = explode($delimiter, $this->message_format);

        $searchfor = ['{', '}', '[', ']'];
        /** @var string[] $cols */
        $cols = str_replace($searchfor, '', $entry_cols);

        foreach ($entries as $entry) {
            $entry = trim($entry);
            /** @var string[] $parts */
            $parts = explode($delimiter, $entry);
            $data[] = [
                $cols[0] => $parts[0],
                $cols[1] => $parts[1],
                $cols[2] => $parts[2],
            ];

        }

        return $data;
    }
}
