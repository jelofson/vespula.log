<?php
declare(strict_types=1);

namespace Vespula\Log\Adapter;

/**
 * Adapter that simply echos the log message to screen
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class None extends AbstractAdapter
{

    /**
     * Do nothing. A simple way to disable logging
     *
     * @param string $level
     * @param string $message
     * @return void
     */
    public function write(string $level, string $message)
    {
    }
}
