<?php
declare(strict_types=1);

namespace Vespula\Log\Adapter;

use Psr\Log\LogLevel;
use function array_merge;

class Chrome extends AbstractAdapter
{

    /**
     * Maps PSR 3 log levels to chrome logger log levels
     *
     * chrome logger = (log) warn, error, info
     * psr 3 = emergency, alert, critical, error, warning, notice, info, debug (log)
     *
     * @var array<string, string>
     */
    protected $loglevel_map = [
        LogLevel::DEBUG=>'info',
        LogLevel::INFO=>'info',
        LogLevel::NOTICE=>'warn',
        LogLevel::WARNING=>'warn',
        LogLevel::ERROR=>'error',
        LogLevel::CRITICAL=>'error',
        LogLevel::ALERT=>'error',
        LogLevel::EMERGENCY=>'error'
    ];

    /**
     * @var string
     * @phpstan-var class-string
     */
    protected $extension;

    /**
     * The chrome extension uses simple static methods, so just send the class name for DI control.
     *
     * This could be more elegant, but it's minor for this adapter.
     *
     * @param string $extension The class name of the chrome extension (for DI)
     * @phpstan-param class-string $extension
     */
    public function __construct(string $extension = '\ChromePhp')
    {
        // check for support of extension?
        $this->extension = $extension;
    }

    /**
     * @param array<string, string> $map
     * @return void
     */
    public function setLoglevelMap(array $map)
    {
        $this->loglevel_map = array_merge($this->loglevel_map, $map);
    }

    /**
     * @return array<string, string>
     */
    public function getLoglevelMap(): array
    {
        return $this->loglevel_map;
    }

    /**
     * @param string $level
     * @param string $message
     * @return void
     */
    public function write(string $level,string $message)
    {
        $timestamp = $this->getTimestamp();
        $message = $this->buildMessage($level, $message, $timestamp);
        $method = $this->loglevel_map[$level];
        $chromephp = $this->extension;
        $chromephp::$method($message);
    }
}
