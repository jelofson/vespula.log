<?php

declare(strict_types=1);

namespace Vespula\Log\Adapter;

use BadMethodCallException;
use Psr\Log\Test\TestLogger as PsrTestLogger;

use function get_class;

/**
 * Class TestLogger
 *
 * @package Vespula\Log\Adapter
 *
 * @method void reset()
 *
 * @method bool hasRecord(string $message, string $level)
 * @method bool hasEmergency(string $message)
 * @method bool hasAlert(string $message)
 * @method bool hasCritical(string $message)
 * @method bool hasError(string $message)
 * @method bool hasWarning(string $message)
 * @method bool hasNotice(string $message)
 * @method bool hasInfo(string $message)
 * @method bool hasDebug(string $message)
 *
 * @method bool hasRecords(string $level)
 * @method bool hasEmergencyRecords()
 * @method bool hasAlertRecords()
 * @method bool hasCriticalRecords()
 * @method bool hasErrorRecords()
 * @method bool hasWarningRecords()
 * @method bool hasNoticeRecords()
 * @method bool hasInfoRecords()
 * @method bool hasDebugRecords()
 *
 * @method bool hasRecordThatContains(string $message, string $level)
 * @method bool hasEmergencyThatContains(string $message)
 * @method bool hasAlertThatContains(string $message)
 * @method bool hasCriticalThatContains(string $message)
 * @method bool hasErrorThatContains(string $message)
 * @method bool hasWarningThatContains(string $message)
 * @method bool hasNoticeThatContains(string $message)
 * @method bool hasInfoThatContains(string $message)
 * @method bool hasDebugThatContains(string $message)
 *
 * @method bool hasRecordThatMatches(string $regex, string $level)
 * @method bool hasEmergencyThatMatches(string $regex)
 * @method bool hasAlertThatMatches(string $regex)
 * @method bool hasCriticalThatMatches(string $regex)
 * @method bool hasErrorThatMatches(string $regex)
 * @method bool hasWarningThatMatches(string $regex)
 * @method bool hasNoticeThatMatches(string $regex)
 * @method bool hasInfoThatMatches(string $regex)
 * @method bool hasDebugThatMatches(string $regex)
 *
 * @method bool hasRecordThatPasses(callable $callable, string $level)
 * @method bool hasEmergencyThatPasses(callable $callable)
 * @method bool hasAlertThatPasses(callable $callable)
 * @method bool hasCriticalThatPasses(callable $callable)
 * @method bool hasErrorThatPasses(callable $callable)
 * @method bool hasWarningThatPasses(callable $callable)
 * @method bool hasNoticeThatPasses(callable $callable)
 * @method bool hasInfoThatPasses(callable $callable)
 * @method bool hasDebugThatPasses(callable $callable)
 */
class TestLogger extends AbstractAdapter
{

    /** @var PsrTestLogger */
    protected $testLogger;

    public function __construct()
    {
        $this->testLogger = new PsrTestLogger();
    }

    /**
     * @inheritDoc
     * @return void
     */
    public function write($level, $message)
    {
        $this->testLogger->log($level, $message);
    }

    /**
     * @param string            $method
     * @param array<int, mixed> $args
     *
     * @return mixed
     * @throws BadMethodCallException
     */
    public function __call(string $method, array $args)
    {
        try {
            return $this->testLogger->{$method}(...$args);
        } catch (BadMethodCallException $ex) {
            throw new BadMethodCallException('Call to undefined method ' . get_class($this) . '::' . $method . '()');
        }
    }

}
