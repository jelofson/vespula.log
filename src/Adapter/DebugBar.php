<?php
declare(strict_types=1);

namespace Vespula\Log\Adapter;

use DebugBar\DataCollector\MessagesCollector;

/**
 * This adapter sends messages to phpdebugbar. See the phpdebugbar docs for more
 * information
 *
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class DebugBar extends AbstractAdapter
{
    /** @var MessagesCollector */
    protected $collector;

    public function __construct(MessagesCollector $collector)
    {
        $this->collector = $collector;
    }

    /**
     * @param string $level
     * @param string $message
     * @return void
     */
    public function write(string $level, string $message)
    {
        $timestamp = $this->getTimestamp();
        $message = $this->buildMessage($level, $message, $timestamp);
        $this->collector->log($level, $message);
    }
}
