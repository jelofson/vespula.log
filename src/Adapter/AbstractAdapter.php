<?php

declare(strict_types=1);

namespace Vespula\Log\Adapter;

use DateTime;
use Psr\Log\LogLevel;
use ReflectionClass;
use Vespula\Log\Exception\InvalidArgumentException;

use function in_array;
use function strtoupper;

/**
 * An abstract adapter implementing the AdapterInterface.
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
abstract class AbstractAdapter implements AdapterInterface
{

    /**
     *
     * @var string The default date format used in the php date() function
     */
    protected $date_format = 'c';

    /**
     * @var string Optional. Set the default timezone for logging. Must be compatible with \DateTimeZone::__construct(string $timezone)
     * 
     */
    protected $timezone = null;

    /**
     * @var string The default message format
     */
    protected $message_format = "[{timestamp}]\t[{level}]\t{message}";

    /**
     * Min log level. DEBUG is of least concern
     *
     * Order is debug < info < notice < warning < error < critical < alert < emergency
     *
     * @var string The minimum level required for logging to occur
     */
    protected $min_level = LogLevel::DEBUG;

    /**
     * Get current date format
     *
     * @return string
     */
    public function getDateFormat(): string
    {
        return $this->date_format;
    }

    /**
     * Set the date format
     *
     * @param string $format
     * @return void
     */
    public function setDateFormat(string $format)
    {
        $this->date_format = $format;
    }

    /**
     * Get current message format
     *
     * @return string
     */
    public function getMessageFormat(): string
    {
        return $this->message_format;
    }

    /**
     * Set the message format
     *
     * @param string $format
     * @return void
     */
    public function setMessageFormat(string $format)
    {
        $this->message_format = $format;
    }

    /**
     * Set the timezone. Must comply with \DateTimeZone::__construct()
     * 
     * @param string $timezone
     * @return void
     */
    public function setTimezone(string $timezone): void
    {
        $this->timezone = $timezone;
    }

    /**
     * Replace placeholders with values in the message format
     *
     * @param string $level
     * @param string $message
     * @param string $timestamp
     * @return string Formatted message
     */
    protected function buildMessage(string $level,string $message,string $timestamp): string
    {
        $replace_pairs = [
            '{timestamp}' => $timestamp,
            '{level}'     => strtoupper($level),
            '{message}'   => $message,
        ];

        return strtr($this->message_format, $replace_pairs);
    }

    /**
     * Get a current timestamp using \DateTime and formatted
     * via \DateTime::format().
     *
     * @return string Formatted timestamp
     *
     * @see AbstractAdapter::setDateFormat()
     */
    public function getTimestamp(): string
    {
        $DateTimeZone = null;
        if ($this->timezone) {
            $DateTimeZone = new \DateTimeZone($this->timezone);
        }
        $date_time = new DateTime("now", $DateTimeZone);
        

        return $date_time->format($this->date_format);
    }

    /**
     * @return string The minimum urgency for logging
     */
    public function getMinLevel(): string
    {
        return $this->min_level;
    }

    /**
     * @param string $level The minimum urgency level for logging
     * @return void
     * @throws InvalidArgumentException
     */
    public function setMinLevel(string $level)
    {
        $reflector    = new ReflectionClass('\Psr\Log\LogLevel');
        $valid_levels = $reflector->getConstants();

        if (!in_array($level, $valid_levels)) {
            throw new InvalidArgumentException(
                'Trying to set an invalid minimum level. Must follow PSR log levels'
            );
        }

        $this->min_level = $level;
    }
}
