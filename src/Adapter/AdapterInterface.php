<?php
declare(strict_types=1);

namespace Vespula\Log\Adapter;

/**
 * Simple interface
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
interface AdapterInterface
{

    /**
     * Write the log message to an adapter-specific target
     *
     * @param string $level
     * @param string $message
     * @return mixed|void
     */
    public function write(string $level, string $message);
}

